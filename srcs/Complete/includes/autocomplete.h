/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   autocomplete.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 23:00:39 by olysogub          #+#    #+#             */
/*   Updated: 2014/03/12 19:08:26 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AUTOCOMPLETE_H
# define AUTOCOMPLETE_H

#include <stdio.h>

typedef struct		s_pathinfos
{
	char		*filename;
	char		*dir;
}					t_pathinfos;

typedef struct		s_dfile
{
	char		*name;
	char		type;
	struct s_dfile	*next;
}					t_dfile;

t_dfile		*ft_autocomplete(char **line, int *pos);

t_dfile		*ft_new_dfile(char *name, char type);
t_dfile		*ft_add_dfile(t_dfile *list, t_dfile *item);
void		ft_free_dfile_list(t_dfile **list);
void		ft_sort_dfile(t_dfile *list);

t_dfile		*ft_complete_line(char **line, int *pos, t_pathinfos *pi);

void		ft_creat_line(char **line, int *pos, char *match, t_pathinfos *pi);

#endif
