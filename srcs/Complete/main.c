/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 23:06:08 by olysogub          #+#    #+#             */
/*   Updated: 2014/03/09 18:41:07 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "autocomplete.h"
#include "libft.h"
#include <stdio.h>

int		main(int ac, char **av)
{
	char		*line;
	int			pos;
	t_dfile		*files;

	if (ac > 2)
	{
		line = ft_strdup(av[1]);
		pos = ft_atoi(av[2]);
		printf("\033[32mLigne de depart :\n");
		printf(">%s<  et pos = %d \n", line, pos);
		/*
		** FONCTION A LANCER
		*/
		files = ft_autocomplete(&line, &pos);
		/*
		** FIN FONCTION A LANCER
		*/
		printf("\033[33mNouvelle ligne :\n");
		printf(">%s<  et pos = %d \n", line, pos);
		printf("\033[34mAffichage de tous les fichiers du dossier :\n");
		while (files)
		{
			printf("%s \n", files->name);
			files = files->next;
		}
		free(line);
		ft_free_dfile_list(&files); // Vider la liste chainee avec tousl es fichiers
		printf("\033[0m----------------\n");
	}
	else
		printf("Usage : [string] [pos] \n");
	return (0);
}
