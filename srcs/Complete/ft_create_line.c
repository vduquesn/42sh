/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 22:55:29 by olysogub          #+#    #+#             */
/*   Updated: 2014/03/12 23:18:32 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "autocomplete.h"
#include "libft.h"
#include <stdlib.h>

/*
** Free the previous line and allocates the new one by setting the new position
*/
void	ft_creat_line(char **line, int *pos, char *match, t_pathinfos *pi)
{
	char		*line_n;
	int			len;
	int			i;
	int			len_filename;
	int			len_match;

	len_filename = ft_strlen(pi->filename);
	len_match = ft_strlen(match);
	len = ft_strlen(*line) - len_filename + len_match;
	line_n = ft_strnew(len);
	ft_strncpy(line_n, *line, (*pos - len_filename));
	i = -1;
	while (match[++i])
	{
		line_n[i + *pos - len_filename] = match[i];
	}
	i = *pos - 1;
	while ((*line)[++i])
		line_n[len_match + i - len_filename] = (*line)[i];
	free(*line);
	*line = line_n;
	*pos = *pos - len_filename + len_match;
}
