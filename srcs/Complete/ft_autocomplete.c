/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomplete.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 23:04:57 by olysogub          #+#    #+#             */
/*   Updated: 2014/03/12 23:53:20 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "autocomplete.h"
#include "libft.h"
#include <stdlib.h>

/*
** Gives the file name of the path (line) going to the position (pos)
*/
char		*ft_get_filename(char *line, int *pos)
{
	char		*filename;
	int			i;
	int			len;

	i = *pos - 1;
	len = 0;
	while (i >= 0 && line[i] != '/' && line[i] != ' ')
	{
		i--;
		len++;
	}
	i += (line[i] == '/' || line[i] == ' ' || i < 0) ? 1 : 0;
	filename = ft_strsub(line, i, len);
	*pos = i;
	return (filename);
}

/*
** Gives the path to the current dir of the last argument
** The position is the begin of the file name
*/
char		*ft_get_dir(char *line, int pos)
{
	char		*dir;
	int		i;
	int		len;

	i = pos - 1;
	len = 0;
	while ((i >= 0))
	{
		if ((line[i] == ' '))
			break ;
		i--;
		len++;
	}
	i += (line[i] == ' ' || i < 0) ? 1 : 0;
	dir = (len > 0) ? ft_strsub(line, i, len) : ft_strdup(".");
	return (dir);
}

/*
** Allocates the t_pathinfo structure and set the filename and the file dir
*/
t_pathinfos	*ft_pathinfos(char *line, int pos)
{
	t_pathinfos	*pi;

	pi = (t_pathinfos *)ft_memalloc(sizeof(t_pathinfos));
	if (pi)
	{
		pi->filename = ft_get_filename(line, &pos);
		pi->dir = ft_get_dir(line, pos);
	}
	return (pi);
}

/*
** Starts the autocompletion algorithme
*/
t_dfile		*ft_autocomplete(char **line, int *pos)
{
	t_pathinfos	*pi;
	t_dfile		*files_list;

	pi = ft_pathinfos(*line, *pos);
	files_list = ft_complete_line(line, pos, pi);
	free(pi->filename);
	free(pi->dir);
	free(pi);
	ft_sort_dfile(files_list);
	return (files_list);
}
