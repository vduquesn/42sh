/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_complete_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 23:12:05 by olysogub          #+#    #+#             */
/*   Updated: 2014/03/13 14:02:23 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "autocomplete.h"
#include "libft.h"
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

/*
** Compares the n first letters of the parameters ->name of the list
*/
int			ft_listncmp(t_dfile *list, int n)
{
	t_dfile		*current;

	if (list)
	{
		current = list->next;
		while (current)
		{
			if (ft_strncmp(list->name, current->name, n) != 0)
				return (0);
			list = current;
			current = current->next;
		}
	}
	return (1);
}

/*
** Returns the max lenght of the (char *) ->name argument of the t_dfile list
*/
int			ft_max_dfile_len(t_dfile *list)
{
	int			len;
	int			max;

	max = 0;
	if (list)
	{
		max = ft_strlen(list->name);
		list = list->next;
		while (list)
		{
			len = ft_strlen(list->name);
			if (len > max)
				max = len;
			list = list->next;
		}
	}
	return (max);
}

/*
 ** Returns the file name if the file is alone or the max matching string of the list
 */
char		*ft_get_direct_match(t_pathinfos *pi, t_dfile *list)
{
	int				i;
	int				list_len;
	t_dfile			*item;
	t_dfile			*cpy;

	i = 0;
	list_len = 0;
	cpy = list;
	while (ft_strlen(pi->filename) == 0 && cpy)
	{
		if (ft_strequ(cpy->name, ".") == 0 && ft_strequ(cpy->name, "..") == 0)
		{
			list_len++;
			item = cpy;
		}
		cpy = cpy->next;
	}
	if (list_len == 1)
		return (ft_strdup(item->name));
	while (ft_listncmp(list, (ft_strlen(pi->filename) + i))
			&& (((int)ft_strlen(pi->filename) + i) <= ft_max_dfile_len(list)))
		i++;
	i -= (i) ? 1 : 0;
	return (ft_strsub(list->name, 0, (ft_strlen(pi->filename) + i)));
}

/*
** Returns the maximal part of the string which matches all
** the ->name of the t_dfile list
*/
char		*ft_get_max_match(t_pathinfos *pi, t_dfile *list)
{
	char		*match;
	char		*cpy;

	match = NULL;
	if (list)
	{
		match = ft_get_direct_match(pi, list);
		if (list->next == NULL && list->type == DT_DIR)
		{
			cpy = ft_strjoin(match, "/");
			free(match);
			match = cpy;
		}
	}
	return (match);
}

/*
** Open the dir of the file and creates the list of the files inside
*/
t_dfile		*ft_complete_line(char **line, int *pos, t_pathinfos *pi)
{
	DIR				*dir;
	struct dirent	*f;
	t_dfile			*list;
	int				len;
	char			*match;

	dir = opendir(pi->dir);
	list = NULL;
	if (dir)
	{
		len = ft_strlen(pi->filename);
		while ((f = readdir(dir)) != NULL)
		{
			if (ft_strncmp(f->d_name, pi->filename, len) == 0)
				list = ft_add_dfile(list, ft_new_dfile(f->d_name, f->d_type));
		}
		closedir(dir);
		match = ft_get_max_match(pi, list);
		if (!match)
			match = ft_strdup(pi->filename);
		ft_creat_line(line, pos, match, pi);
		free(match);
	}
	return (list);
}
