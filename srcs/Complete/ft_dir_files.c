/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dir_files.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 23:12:36 by olysogub          #+#    #+#             */
/*   Updated: 2014/03/11 14:36:32 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "autocomplete.h"
#include "stdio.h"

t_dfile		*ft_new_dfile(char *name, char type)
{
	t_dfile		*file;

	file = (t_dfile *)ft_memalloc(sizeof(t_dfile));
	if (file)
	{
		file->name = ft_strdup(name);
		file->type = type;
	}
	return (file);
}

t_dfile		*ft_add_dfile(t_dfile *list, t_dfile *item)
{
	if (item)
	{
		item->next = list;
		return (item);	
	}
	else
		return (list);
}

void		ft_free_dfile_list(t_dfile **list)
{
	if (list && *list)
	{
		ft_free_dfile_list(&(*list)->next);
		free((*list)->name);
		free(*list);
	}
}

void		ft_sort_dfile(t_dfile *list)
{
	t_dfile			cpy;
	t_dfile			*current;
	t_dfile			*current2;

	current = list;
	while (current)
	{
		current2 = current->next;
		while (current2)
		{
			if (ft_strcmp(current2->name, current->name) < 0)
			{
				cpy.name = current2->name;
				cpy.type = current2->type;
				current2->name = current->name;
				current2->type = current->type;
				current->name = cpy.name;
				current->type = cpy.type;
			}
			current2 = current2->next;
		}
		current = current->next;
	}
}