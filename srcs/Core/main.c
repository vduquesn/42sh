/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vduquesn <vduquesn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/05 00:39:55 by vduquesn          #+#    #+#             */
/*   Updated: 2014/03/12 17:24:50 by vduquesn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"
#include "libft.h"

static void	raw_terminal_mode(int active)
{
	static struct termios	old;
	static struct termios	new;
	static int				is_active;

	if (is_active != active)
	{
		if (active)
		{
			tcgetattr(0, &old);
			new = old;
			new.c_lflag &= ~(ICANON | ECHO);
			new.c_cc[VMIN] = 1;
			new.c_cc[VTIME] = 0;
			tcsetattr(0, 0, &new);
		}
		else
			tcsetattr(0, 0, &old);
		is_active = active;
	}
}

static void	display_promptline(char *line, int *position)
{
	tputs(tgetstr("cr", NULL), 1, my_putchar);
	tputs(tgetstr("ce", NULL), 1, my_putchar);
	write(1, "$>", 2);
	write(1, line, ft_strlen(line));
	tputs(tgoto(tgetstr("ch", NULL), 0, *position + 2), 1, my_putchar);
}

static int	ft_init_term(void)
{

	if (tgetent(NULL, getenv("TERM")) == 1)
		signal(SIGINT, catch_signal);
	else
	{
		write(0, "Error : couldn't access the termcap database.\n", 46);
		return (1);
	}
	raw_terminal_mode(1);
	write(1, "$>", 2);
	return (0);
}

void		catch_signal(int sign)
{
	struct termios	term;

	tcgetattr(0, &term);
	if (sign == SIGINT)
		tcsetattr(0, 0, &term);
}

int			main(void)
{
	char	buff[4];
	char	*line;
	int		key;
	int		position;

	if ((position = ft_init_term()) == 1)
		return (0);
	line = ft_strnew(0);
	ft_bzero(buff, 3);
	while (read(0, buff, 4))
	{
		//printf("%d %d %d %d\n", buff[0], buff[1], buff[2], buff[3]);
		key = ft_getkey(buff);
		ft_actionkey(key, &line, &position);
		ft_bzero(buff, 4);
		display_promptline(line, &position);
		//printf("position : %d\n", position);
	}
	raw_terminal_mode(0);
	return (0);
}
