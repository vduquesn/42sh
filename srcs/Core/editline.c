/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   editline.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vduquesn <vduquesn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/05 01:02:32 by vduquesn          #+#    #+#             */
/*   Updated: 2014/03/12 18:15:24 by vduquesn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"
#include "libft.h"
#include "autocomplete.h"

int			ft_getkey(char *buff)
{
	if (buff[0] == 27 && buff[1] == 91 && buff[2] == 68 && !buff[3])
		return (300);
	else if (buff[0] == 27 && buff[1] == 91 && buff[2] == 65 && !buff[3])
		return (301);
	else if (buff[0] == 27 && buff[1] == 91 && buff[2] == 67 && !buff[3])
		return (302);
	else if (buff[0] == 27 && buff[1] == 91 && buff[2] == 66 && !buff[3])
		return (303);
	else if (buff[0] == 10 && !buff[1] && !buff[2] && !buff[3])
		return (304);
	else if (buff[0] == 127 && !buff[1] && !buff[2] && !buff[3])
		return (305);
	else if (buff[0] == 9 && !buff[1] && !buff[2] && !buff[3])
		return (306);
	else if (buff[0] == 1 && !buff[1] && !buff[2] && !buff[3])
		return (307);
	else if (buff[0] == 5 && !buff[1] && !buff[2] && !buff[3])
		return (308);
	else if (buff[0] == 27 && !buff[1] && !buff[2] && !buff[3])
		return (309);
	else if (buff[0] == 23 && !buff[1] && !buff[2] && !buff[3])
		return(310);
	else if (buff[0] == 24 && !buff[1] && !buff[2] && !buff[3])
		return (311);
	return (buff[0]);
}

static void	move_arrow(int key, int *position, char **line)
{
	int	len;

	len = ft_strlen(*line);
	if (*position && key == 300)
		*position = *position - 1;
	else if (*position < len && key == 302 && *position < tgetnum("co") - 1)
		*position = *position + 1;
}

static void	ft_movetolimit(int key, int *position, char *line)
{
	if (key == 307)
		*position = 0;
	else if (key == 308)
		*position = ft_strlen(line);
}

static void	ft_previousword(int *position, char *s)
{
	int	word;

	word = 0;
	while (*position && (!word || s[*position - 1] != ' '))
	{
		word = (s[*position] != ' ') ? 1 : word;
		*position = *position - 1;
	}
}

static void	ft_nextword(int *position, char *s)
{
	int	i;
	int	space;

	i = *position;
	space = 0;
	while (s[i] && (!space || s[i] == ' '))
	{
		space = (s[i] == ' ') ? 1 : space;
		i++;
	}
	*position = (s[i]) ? i : *position;
}

void	ft_actionkey(int key, char **line, int *position)
{
	char c;

	c = (key < 300) ? key : 0;
	if (c)
		*line = ft_addctos(line, c, position);
	else if (key == 300 || key == 302)
		move_arrow(key, position, line);
	else if (key == 301 || key == 303)
		;
		//historique oleksiy
	else if (key == 305)
		ft_delctos(*line, position);
	else if (key == 306)
		ft_autocomplete(line, position);
	else if (key == 307 || key == 308)
		ft_movetolimit(key, position, *line);
	else if (key == 304)
		ft_exit(304, line, position);
	else if (key == 309)
		ft_exit(309, line, position);
	else if (key == 310)
		ft_nextword(position, *line);
	else if (key == 311)
		ft_previousword(position, *line);
}
