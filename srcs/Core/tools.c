/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vduquesn <vduquesn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/05 06:06:36 by vduquesn          #+#    #+#             */
/*   Updated: 2014/03/12 18:13:27 by vduquesn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"
#include "libft.h"

char	*ft_addctos(char **s, char c, int *position)
{
	char	*new;
	int		i;
	int		len;

	if (!*s)
		return (ft_strnew(0));
	len = ft_strlen(*s);
	new = (char *)malloc(sizeof(char) * (len + 2));
	i = 0;
	while ((*s)[i] && i < *position)
	{
		new[i] = (*s)[i];
		i++;
	}
	new[i] = c;
	i--;
	while ((*s)[++i])
		new[i + 1] = (*s)[i];
	new[i + 1] = 0;
	free(*s);
	if (*position < tgetnum("co"))
		*position = *position + 1;
	return (new);
}

void	ft_exit(int key, char **line, int *position)
{
	write(1, "\n", 1);
	if (key == 304)
	{
		//ici on envoit au parser
		free(*line);
		*position = 0;
		*line = ft_strnew(0);
	}
	else if (key == 309)
	{
		free(*line);
		exit(0);
	}
}

void	ft_delctos(char *s, int *position)
{
	int		i;

	if (*position && s)
	{
		i = 0;
		while (i < (*position - 1))
			i++;
		while (s[++i])
			s[i - 1] = s[i];
		s[i - 1] = 0;
		*position = *position - 1;
	}
}

int		my_putchar(int c)
{
	write(1, &c, 1);
	return (1);
}
