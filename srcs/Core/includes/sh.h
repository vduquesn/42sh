/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vduquesn <vduquesn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/05 00:42:24 by vduquesn          #+#    #+#             */
/*   Updated: 2014/03/12 18:14:47 by vduquesn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_H
# define SH_H

# include <termcap.h>
# include <termios.h>
# include <term.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h> //WARNING

int		ft_getkey(char *buff);
void	ft_actionkey(int key, char **line, int *position);
char	*ft_addctos(char **s, char c, int *position);
void	ft_delctos(char *s, int *position);
int		my_putchar(int c);
void	catch_signal(int sign);
void	action_leftarrow(char *line, int *position);
void	ft_exit(int key, char **line, int *position);

#endif /* !SH_H */
