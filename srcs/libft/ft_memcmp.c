/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 11:27:55 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/29 15:45:17 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned int					i;
	unsigned char					*s1_cpy;
	unsigned char					*s2_cpy;
	int								diff;

	i = 0;
	s1_cpy = (unsigned char *)s1;
	s2_cpy = (unsigned char *)s2;
	diff = 0;
	while (i < n)
	{
		diff = s1_cpy[i] - s2_cpy[i];
		if (diff)
		{
			return (diff);
		}
		i++;
	}
	return (diff);
}
