/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 11:27:55 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/28 20:51:42 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	unsigned int			i;
	unsigned int			len;

	i = 0;
	while (i < dstsize && dst[i])
		i++;
	if (i == dstsize)
		return (dstsize + (ft_strlen(src)));
	i = 0;
	len = ft_strlen(dst);
	while (src[i] && (len + i) < (dstsize - 1))
	{
		dst[len + i] = src[i];
		i++;
	}
	dst[len + i] = '\0';
	return (len + ft_strlen(src));
}
