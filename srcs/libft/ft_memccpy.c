/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 15:32:59 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/29 15:41:34 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	unsigned int							i;
	unsigned char							*s1_c;
	const unsigned char						*s2_c;

	if (!s1 || !s2 || !n)
		return (NULL);
	i = 0;
	s1_c = (unsigned char *)s1;
	s2_c = (unsigned char *)s2;
	while (i < n)
	{
		s1_c[i] = s2_c[i];
		if ((unsigned char)c == s1_c[i])
			return (s1 + i + 1);
		i++;
	}
	return (NULL);
}
