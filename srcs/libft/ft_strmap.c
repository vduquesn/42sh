/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 12:25:01 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/25 12:25:03 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strmap(char const *s, char (*f)(char))
{
	int			i;
	char		*cpy;

	i = 0;
	cpy = NULL;
	if (s != NULL)
	{
		cpy = ft_strnew(ft_strlen(s));
		if (cpy == NULL)
		{
			return (NULL);
		}
		while (s[i])
		{
			cpy[i] = (f) ? (*f)(s[i]) : s[i];
			i++;
		}
		return (cpy);
	}
	return (cpy);
}
