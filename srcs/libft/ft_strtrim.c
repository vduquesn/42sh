/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/30 16:17:41 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_iswhitespace(int c)
{
	return ((c == ' ') || (c == '\n') || (c == '\t'));
}

char		*ft_strtrim(char const *s)
{
	int			i;
	int			ss;
	int			se;
	int			lenght;

	if (!s)
		return (NULL);
	ss = 0;
	se = 0;
	lenght = ft_strlen(s);
	while (ft_iswhitespace(s[ss]) && s[ss])
	{
		ss++;
	}
	i = lenght - 1;
	while (ft_iswhitespace(s[i]) && i >= 0)
	{
		se++;
		i--;
	}
	lenght = ((ss == se) && (se == lenght)) ? ss : (lenght - se - ss);
	return (ft_strsub(s, ss, lenght));
}
