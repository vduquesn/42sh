/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 11:27:55 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/28 17:45:40 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	unsigned int			i;
	char					*s1_c;
	const char				*s2_c;

	if (!s1 || !s2 || !n)
		return (s1);
	i = 0;
	s1_c = (char *)s1;
	s2_c = (char *)s2;
	while (i < n)
	{
		s1_c[i] = s2_c[i];
		i++;
	}
	return (s1_c);
}
