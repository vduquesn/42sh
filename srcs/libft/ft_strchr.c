/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/30 16:27:29 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strchr(const char *s, int c)
{
	int			i;
	char		*str_c;

	str_c = (char *)s;
	i = 0;
	while (s[i] != '\0')
	{
		if (s[i] == c)
		{
			return (&str_c[i]);
		}
		i++;
	}
	if (s[i] == c)
	{
		return (&str_c[i]);
	}
	return (NULL);
}
