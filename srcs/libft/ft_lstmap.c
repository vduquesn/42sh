/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 19:02:25 by olysogub          #+#    #+#             */
/*   Updated: 2013/12/07 19:02:33 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list			*cpy;
	t_list			*l;

	cpy = NULL;
	if (!lst && !f)
		return (NULL);
	while (lst)
	{
		if (cpy)
		{
			l->next = (*f)(lst);
			l = l->next;
		}
		else
		{
			cpy = (*f)(lst);
			if (!cpy)
				return (NULL);
			l = cpy;
		}
		lst = lst->next;
	}
	return (cpy);
}
