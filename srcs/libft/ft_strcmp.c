/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 11:27:55 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/28 21:07:09 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_strcmp(const char *s1, const char *s2)
{
	int			i;
	int			res;

	i = 0;
	while (s1[i] && s2[i])
	{
		res = s1[i] - s2[i];
		if (res != 0)
			return (res);
		i++;
	}
	if (s1[i] && !s2[i])
		return (s1[i]);
	else if (!s1[i] && s2[i])
		return (-s2[i]);
	else
		return (0);
}
