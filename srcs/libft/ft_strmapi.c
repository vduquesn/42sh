/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 12:30:53 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/25 12:30:55 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int			i;
	char		*cpy;

	i = 0;
	cpy = NULL;
	if (s != NULL)
	{
		cpy = ft_strnew(ft_strlen(s));
		if (cpy == NULL)
		{
			return (NULL);
		}
		while (s[i])
		{
			cpy[i] = (f) ? (*f)(i, s[i]) : s[i];
			i++;
		}
		return (cpy);
	}
	return (cpy);
}
