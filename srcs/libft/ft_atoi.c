/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/30 16:12:35 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		i;
	int		nbr;
	int		positif;
	int		power;

	i = 0;
	nbr = 0;
	power = 0;
	while (ft_isspace(str[i]))
		i++;
	positif = (str[i] == '-') ? -1 : 1;
	i += (str[i] == '-' || str[i] == '+') ? 1 : 0;
	while (ft_isdigit(str[i + power]) && str[i + power])
	{
		power++;
	}
	if (power == 0)
		return (0);
	while (ft_isdigit(str[i]) && str[i])
	{
		nbr += ft_pow(10, power - 1) * (str[i] - 48);
		power--;
		i++;
	}
	return (positif * nbr);
}
