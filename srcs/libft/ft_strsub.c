/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/30 16:14:50 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strsub(char const *s, unsigned int start, size_t len)
{
	int			i;
	char		*cpy;
	int			lenght;
	int			j;

	if (!s)
		return (NULL);
	i = start;
	j = 0;
	cpy = ft_strnew(len);
	lenght = start + len;
	if (cpy == NULL)
	{
		return (NULL);
	}
	while (i != lenght)
	{
		cpy[j] = s[i];
		i++;
		j++;
	}
	return (cpy);
}
