/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/30 16:18:50 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	char				*str;
	unsigned int		i;
	unsigned int		j;
	unsigned int		len;

	i = 0;
	j = 0;
	str = (char *)s1;
	len = ft_strlen(s2);
	if (!*s2)
		return (str);
	while (s1[i] && (i < n))
	{
		if (s1[i] == s2[j])
			j++;
		else
		{
			i = i - j;
			j = 0;
		}
		if (j == len)
			return (&str[i - j + 1]);
		i++;
	}
	return (NULL);
}
