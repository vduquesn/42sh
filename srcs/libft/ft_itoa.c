/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/30 16:13:52 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_countpower(int n, int *len, int *i)
{
	int		power;

	power = 0;
	*i = 0;
	while (n != 0)
	{
		power++;
		n = n / 10;
	}
	*len = (n >= 0) ? (power + 1) : (power + 2);
	return (power);
}

char			*ft_itoa(int n)
{
	int		i;
	int		power;
	char	*nbr;
	int		len;

	power = ft_countpower(n, &len, &i);
	nbr = ft_strnew(len);
	if (nbr == NULL)
		return (NULL);
	if (n == -2147483648)
		return (ft_strcpy(nbr, "-2147483648"));
	nbr[0] = (n < 0) ? '-' : '\0';
	i = (n < 0) ? (i + 1) : i;
	n = (n < 0) ? (n * (-1)) : n;
	while (power > 1)
	{
		nbr[i] = (n / ft_pow(10, power - 1)) + 48;
		n = n - (n / ft_pow(10, power - 1)) * ft_pow(10, power - 1);
		power--;
		i++;
	}
	nbr[i] = (n + 48);
	return (nbr);
}
