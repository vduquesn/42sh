/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/30 16:30:08 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memchr(const void *s, int c, size_t n)
{
	unsigned int					i;
	unsigned char					*s_cpy;

	i = 0;
	s_cpy = (unsigned char*)s;
	if (!s || !n)
		return (NULL);
	while (i < n)
	{
		if ((unsigned char)c == s_cpy[i])
		{
			return (s_cpy + i);
		}
		i++;
	}
	return (NULL);
}
