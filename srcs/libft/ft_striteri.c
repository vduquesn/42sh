/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 12:21:47 by olysogub          #+#    #+#             */
/*   Updated: 2013/11/25 12:21:48 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	int			i;

	if (s != NULL && f !=  NULL)
	{
		i = 0;
		while (s[i])
		{
			f(i, (s + i));
			i++;
		}
	}
}
