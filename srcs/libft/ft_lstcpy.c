/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 18:57:41 by olysogub          #+#    #+#             */
/*   Updated: 2013/12/07 19:00:49 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static void			st_delcontent(void *content, size_t content_size)
{
	ft_bzero(content, content_size);
	ft_memdel(content);
}

t_list				*ft_lstcpy(t_list *list)
{
	t_list		*list_cpy;
	t_list		*l;

	list_cpy = NULL;
	if (!list)
		return (NULL);
	while (list)
	{
		l = ft_lstnew(list->content, list->content_size);
		if (!l)
		{
			ft_lstdel(&list_cpy, &st_delcontent);
			return (NULL);
		}
		if (list_cpy)
		{
			list_cpy->next = l;
		}
		else
		{
			list_cpy = l;
		}
		list = list->next;
	}
	return (list_cpy);
}
