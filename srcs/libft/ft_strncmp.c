/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 11:34:22 by olysogub          #+#    #+#             */
/*   Updated: 2014/03/04 14:47:39 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned int	i;
	int				res;

	i = 0;
	while (s1[i] && s2[i] && (i < n))
	{
		res = s1[i] - s2[i];
		if (res != 0)
			return (res);
		i++;
	}
	if (i < n)
	{
		if (s1[i] && !s2[i])
			return (s1[i]);
		else if (!s1[i] && s2[i])
			return (-s2[i]);
	}
	return (0);
}
