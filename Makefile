# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vduquesn <vduquesn@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/03/05 00:39:49 by vduquesn          #+#    #+#              #
#    Updated: 2014/03/12 18:10:10 by vduquesn         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 42sh
CC = gcc -Wall -Wextra -Werror -g -L /usr/lib -l termcap
SOURCES = srcs/Core/main.c \
		  srcs/Core/editline.c \
		  srcs/Core/tools.c \
		  srcs/Complete/ft_autocomplete.c \
		  srcs/Complete/ft_complete_line.c \
		  srcs/Complete/ft_create_line.c \
		  srcs/Complete/ft_dir_files.c 

INCLUDES = -I srcs/Core/includes -I srcs/Complete/includes -I srcs/libft

LIBS = -L srcs/libft -lft

OBJECTS = $(SOURCES:.c=.o)

.PHONY: all clean fclean re

$(NAME): $(OBJECTS)
	make -C srcs/libft
	$(CC) -o $(NAME) $(OBJECTS) $(INCLUDES) $(LIBS)

%.o: %.c
	$(CC) -c $< -o $@ $(INCLUDES)

clean:
	rm -f $(OBJECTS)

fclean: clean
	rm -f $(NAME)

re: fclean $(NAME)
